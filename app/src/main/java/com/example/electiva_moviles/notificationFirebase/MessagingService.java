package com.example.electiva_moviles.notificationFirebase;

import android.os.Handler;
import android.os.Looper;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

public class MessagingService extends FirebaseMessagingService {

    @Override
    public void onMessageReceived(@NonNull RemoteMessage remoteMessage) {
        Looper.prepare();

        Handler handler = new Handler();
        handler.post(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(getBaseContext(), remoteMessage.getNotification().getTitle(), Toast.LENGTH_LONG).show();
            }
        });
        Looper.loop();
    }
}
