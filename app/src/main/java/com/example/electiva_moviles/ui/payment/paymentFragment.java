package com.example.electiva_moviles.ui.payment;

import androidx.lifecycle.ViewModelProvider;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.TextView;

import com.payu.base.listeners.HashGenerationListener;
import com.payu.base.models.ErrorResponse;
import com.payu.base.models.PayUPaymentParams;

import com.example.electiva_moviles.R;
import com.payu.checkoutpro.PayUCheckoutPro;
import com.payu.checkoutpro.utils.PayUCheckoutProConstants;
import com.payu.ui.model.listeners.PayUCheckoutProListener;
import com.payu.ui.model.listeners.PayUHashGenerationListener;

import java.util.HashMap;

import kotlin.HashCodeKt;

public class paymentFragment extends Fragment {
    private TextView lblNameProduct;
    private TextView lblCostProduct;
    private TextView lblResultBuy;
    private String dataBuy;
    private Button btbBuy;

    private PaymentViewModel mViewModel;

    public static paymentFragment newInstance() {
        return new paymentFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.payment_fragment, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        lblNameProduct = getActivity().findViewById(R.id.txtNameProduct);
        lblCostProduct = getActivity().findViewById(R.id.txtCostProduct);
        lblResultBuy = getActivity().findViewById(R.id.txtResultPay);
        btbBuy = getActivity().findViewById(R.id.btnBuy);
        btbBuy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dataBuy = buyProductPayu(lblNameProduct.getText().toString(), lblCostProduct.getText().toString());
            }
        });
    }

    private String buyProductPayu(String nameProduct, String costProduct) {
        /*PayUPaymentParams.Builder builder = new PayUPaymentParams.Builder();
        builder.setAmount(costProduct)
        .setIsProduction(true)
        .setProductInfo(nameProduct)
        .setKey("")
        .setPhone("123456789")
        .setTransactionId(String.valueOf(System.currentTimeMillis()))
        .setFirstName("Johan")
        .setEmail("jddiaz@ucatolica.edu.co")
        .setSurl("https://payuresponse.firebaseapp.com/success")
        .setFurl("https://payuresponse.firebaseapp.com/failure");
        PayUPaymentParams payUPaymentParams = builder.build();

        PayUCheckoutPro.open(
                this,
                payUPaymentParams,
                new PayUCheckoutProListener() {

                    @Override
                    public void onPaymentSuccess(Object response) {
                        //Cast response object to HashMap
                        HashMap<String,Object> result = (HashMap<String, Object>) response;
                        String payuResponse = (String)result.get(PayUCheckoutProConstants.CP_PAYU_RESPONSE);
                        String merchantResponse = (String) result.get(PayUCheckoutProConstants.CP_MERCHANT_RESPONSE);
                    }

                    @Override
                    public void onPaymentFailure(Object response) {
                        //Cast response object to HashMap
                        HashMap<String,Object> result = (HashMap<String, Object>) response;
                        String payuResponse = (String)result.get(PayUCheckoutProConstants.CP_PAYU_RESPONSE);
                        String merchantResponse = (String) result.get(PayUCheckoutProConstants.CP_MERCHANT_RESPONSE);
                    }

                    @Override
                    public void onPaymentCancel(boolean isTxnInitiated) {
                    }

                    @Override
                    public void onError(ErrorResponse errorResponse) {
                        String errorMessage = errorResponse.getErrorMessage();
                    }

                    @Override
                    public void setWebViewProperties(@Nullable WebView webView, @Nullable Object o) {
                        //For setting webview properties, if any. Check Customized Integration section for more details on this
                    }

                    @Override
                    public void generateHash(HashMap<String, String> valueMap, PayUHashGenerationListener hashGenerationListener) {
                        String hashName = valueMap.get(PayUCheckoutProConstants.CP_HASH_NAME);
                        String hashData = valueMap.get(PayUCheckoutProConstants.CP_HASH_STRING);
                        if (!TextUtils.isEmpty(hashName) && !TextUtils.isEmpty(hashData)) {
                            //Do not generate hash from local, it needs to be calculated from server side only. Here, hashString contains hash created from your server side.
                            String hash = hashString;
                            HashMap<String, String> dataMap = new HashMap<>();
                            dataMap.put(hashName, hash);
                            hashGenerationListener.onHashGenerated(dataMap);
                        }
                    }
                }
        );*/
        return "";
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = new ViewModelProvider(this).get(PaymentViewModel.class);
        // TODO: Use the ViewModel
    }

}