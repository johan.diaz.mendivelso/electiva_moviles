package com.example.electiva_moviles.ui.encrypt;

import androidx.lifecycle.ViewModelProvider;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.example.electiva_moviles.R;
import com.google.android.material.snackbar.Snackbar;

import java.security.MessageDigest;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

public class encrypyFragment extends Fragment {
    private EditText etData;
    private EditText etPassword;
    private TextView lblDataEncrypt;
    private String dataEncrypt;
    private Button btnEncrypt;
    private Button btnDesencrypt;

    private EncrypyViewModel mViewModel;

    public static encrypyFragment newInstance() {
        return new encrypyFragment();
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.encrypy_fragment, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        etData = getActivity().findViewById(R.id.txtData);
        etPassword = getActivity().findViewById(R.id.txtPass);
        lblDataEncrypt = getActivity().findViewById(R.id.txtReturnEncrypt);
        btnEncrypt = getActivity().findViewById(R.id.btnEncrypt);
        btnEncrypt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    if (etData.length() == 0) {
                        Snackbar.make(v, "LA DATA ESTA VACIA", Snackbar.LENGTH_SHORT).setAction("Action", null).show();
                    } else {
                        dataEncrypt = encriptar(etData.getText().toString(), etPassword.getText().toString());
                        lblDataEncrypt.setText(dataEncrypt);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        btnDesencrypt = getActivity().findViewById(R.id.btnDesencrypt);
        btnDesencrypt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    dataEncrypt = desencriptar(dataEncrypt, etPassword.getText().toString());
                    lblDataEncrypt.setText(dataEncrypt);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });
    }
    private String desencriptar(String dataEncrypt, String password) throws Exception {
        SecretKeySpec secretKey = generateKey(password);
        Cipher cipher = Cipher.getInstance("AES");
        cipher.init(Cipher.DECRYPT_MODE, secretKey);
        byte[] dataDescodificado = Base64.decode(dataEncrypt, Base64.DEFAULT);
        byte[] dataDesencryptByte = cipher.doFinal(dataDescodificado);
        String dataEncryptString = new String(dataDesencryptByte);
        return dataEncryptString;
    }

    private String encriptar(String data, String password) throws Exception {
        SecretKeySpec secretKey = generateKey(password);
        Cipher cipher = Cipher.getInstance("AES");
        cipher.init(Cipher.ENCRYPT_MODE, secretKey);
        byte[] dataEncryptByte = cipher.doFinal(data.getBytes());
        String dataEncryptString = Base64.encodeToString(dataEncryptByte, Base64.DEFAULT);
        return dataEncryptString;
    }

    private SecretKeySpec generateKey(String password) throws Exception{
        MessageDigest SHA = MessageDigest.getInstance("SHA-256");
        byte[] key = password.getBytes("UTF-8");
        key = SHA.digest(key);
        SecretKeySpec secretKeySpec = new SecretKeySpec(key, "AES");
        return secretKeySpec;
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mViewModel = new ViewModelProvider(this).get(EncrypyViewModel.class);
        // TODO: Use the ViewModel
    }

}